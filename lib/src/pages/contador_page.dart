import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget{

@override
  createState() => _ContadorPageState();

}

class _ContadorPageState extends State<ContadorPage>{
int _count = 0;

@override
  Widget build(BuildContext context) {
  
    return Scaffold(
      appBar: AppBar(
       title: Text('AppFlutter Contador'),
      ),
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Numero de Clicks: ', style: TextStyle(fontSize: 25)),
          Text('$_count', style: TextStyle(fontSize: 25)),
        ]
      ),
      ),
   floatingActionButton: _crearBotones(),
    );
}

Widget _crearBotones(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      SizedBox(width:20.0),
      FloatingActionButton(child: Icon(Icons.exposure_zero), onPressed: _reset),
      Expanded(child: SizedBox(width:5.0)),
      FloatingActionButton(child: Icon(Icons.remove), onPressed: _restar),
      SizedBox(width:20.0),
      FloatingActionButton(child: Icon(Icons.add), onPressed: _agregar),
    ],
  );
}

void _agregar(){
  setState(() => _count++);
}
void _restar(){
  setState(() => _count--);
}
void _reset(){
  setState(() => _count=0);
}
}