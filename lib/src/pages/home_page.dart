
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{

@override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
       title: Text('AppFlutter Contador'),
      ),
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Numero de Clicks: ", style: TextStyle(fontSize: 25)),
          Text("0", style: TextStyle(fontSize: 25)),
        ]
      ),
      ),
   floatingActionButton: FloatingActionButton(
     child: Icon(Icons.add),
     onPressed: () {
       
     },
     ),
    );

  }

}
